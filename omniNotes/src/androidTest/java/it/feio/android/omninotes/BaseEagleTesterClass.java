
package it.feio.android.omninotes;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.test.InstrumentationRegistry;
import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

import com.mobileenerlytics.eagle.tester.logger.EagleTester;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import it.feio.android.omninotes.db.DbHelper;
import it.feio.android.omninotes.utils.Constants;


public class BaseEagleTesterClass extends AndroidTestCase {

    public static EagleTester eagleTester;

    @BeforeClass
    public static void setEagleTester() throws InterruptedException {
        eagleTester = new EagleTester(InstrumentationRegistry.getTargetContext());
    }

    @AfterClass
    public static void tearEagleTester() throws InterruptedException {
        eagleTester.finish();
    }
}
